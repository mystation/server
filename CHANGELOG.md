# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2019-07-07
### Added
- Link, and make it obvious that date format is ISO 8601.

### Changed
- Clarified the section on "Is there a standard change log format?".

### Fixed
- Fix Markdown links to tag comparison URL with footnote-style links.

### Deprecated
- Nothing

### Removed
- Nothing

[Unreleased]: https://gitlab.com/mystation/server/compare/v1.0.0...master
[1.0.0]: https://gitlab.com/mystation/server/compare/v0.0.0...v1.0.0