<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppPancake extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'app_name'
    ];

    /**
     * Table name
     * 
     * @var string
     */
    protected $table = "app_pancakes";

    /**
     * The primary key column name.
     *
     * @var string
     */
    protected $primaryKey = 'name';

    /**
     * The type of primary key.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The app
     * 
     * @return App\App
     */
    public function app()
    {
        return $this->belongsTo('App\App', 'name', 'app_name');
    }

    /**
     * The users who have activated pancake
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'user_pancakes', 'pancake_name', 'user_id')->withPivot(['position', 'is_activated']);
    }

    /**
     * Overwrite toArray() method to change name of pivot
     */
    public function toArray()
    {
        $attributes = $this->attributesToArray();
        $attributes = array_merge($attributes, $this->relationsToArray());
        
        // Detect if there is a pivot
        if (isset($attributes['pivot'])) {
            $attributes['position'] = $attributes['pivot']['position'];
            $attributes['is_activated'] = $attributes['pivot']['is_activated'];
            unset($attributes['pivot']);
        }

        return $attributes;
    }

}
