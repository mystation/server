<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AppResource extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'app_name',
        'resource',
        'table_name',
        'prim_key',
        'key_type',
        'incrementing',
        'timestamps_fields',
        'user_related'
    ];

    /**
     * Table name
     * 
     * @var string
     */
    protected $table = "app_resources";

    /**
     * The primary key column name.
     *
     * @var string
     */
    protected $primaryKey = ['app_name', 'resource'];

    /**
     * The incrementing option
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The app
     * 
     * @return App\App[]
     */
    public function app()
    {
        return $this->belongsTo('App\App', 'name', 'app_name');
    }

    /**
     * Create table from app resource configuration
     * 
     * @param array $columns - Columns definitions
     * @param array $foreigns - Foreign keys definitions
     * @return 
     */
    public function createTable($columns, $foreigns) {
        // Create the table based on columns
        $to_eval = '';
        $incrementsField = false;
        foreach ( $columns as $column ) {
            $to_eval .= '$table';
            // Name
            $name = $column['name'];
            // Type
            $type = $column['type'];
            if ( $type === 'increments' ) {
                $incrementsField = true; // Detect an 'increments' type field
            }
            // Adapt to type
            if ( $type === 'enum' ) {
                $options = "[";
                foreach($column['options'] as $value) {
                    $options .= "'$value', ";
                }
                $options .= "]";
                $to_eval .= "->$type('$name', $options)";
            }
            else {
                $to_eval .= "->$type('$name')";
            }
            // Nullable
            if ( isset($column['nullable']) ) {
                $nullable = $column['nullable'];
                $to_eval .= "->nullable()";
            }
            // Default value
            if ( isset($column['default']) ) {
                $default = $column['default'];
                $to_eval .= "->default('$default')";
            }
            // Comment
            if ( isset($column['comment']) ) {
                $comment = addslashes($column['comment']);
                $to_eval .= "->comment('$comment')";
            }
            // Line end
            $to_eval .= ";\n";
        }
        // Foreign keys
        foreach ( $foreigns as $foreign ) {
            $foreignType = $foreign['type'];
            $name = $foreign['foreign'];
            $references = $foreign['references'];
            $on = $foreign['on'];
            $to_eval .= "\$table->$foreignType('$name');\n
                        \$table->foreign('$name')
                                ->references('$references')
                                ->on('" . $this->app_name . "_$on')
                                ->onDelete('cascade');\n";
        }
        // User relation
        if ( $this->user_related ) {
            $to_eval .= "\$table->unsignedBigInteger('user_id');\n
                         \$table->foreign('user_id')
                                ->references('id')
                                ->on('users');\n";
        }
        // Timestamps
        if ( $this->timestamps_fields ) {
            $to_eval .= "\$table->timestamps();\n";
        }
        // Primary key (only if any 'increments' field found)
        if ( !$incrementsField ) {
            if ( is_array($this->prim_key) ) {
                $keys = "[";
                foreach($this->prim_key as $value) {
                    $keys .= "'$value', ";
                }
                $keys .= "]";
                $to_eval .= "\$table->primary($keys);\n";
            } else {
                $to_eval .= "\$table->primary('" . $this->prim_key . "');\n";
            }
        }
        // Create table
        Schema::create($this->table_name, function (Blueprint $table) use ($to_eval) {
            eval($to_eval);
        });
    }

    /**
     * Delete table of an app resource
     * 
     * @return void
     */
    public function deleteTable() {
        // Drop table
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists($this->table_name);
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Duplicate table of an app resource as temporary table for update
     * 
     * @return void
     */
    public function tempTable() {
        $tempName = 'temp_'.$this->table_name;
        DB::unprepared('CREATE TABLE '.$tempName.' LIKE '.$this->table_name);
        DB::insert('INSERT INTO '.$tempName.' SELECT * FROM '.$this->table_name);
    }

    /**
     * OVERWRITE Eloquent Model STATIC find() method (for composite primary key)
     * Find an app resource
     * 
     * @param string $appName - The app name
     * @param string $resource - The resource name
     * @return AppResource
     */
    public static function find($appName, $resource) {
        return AppResource::where([
            'app_name' => $appName,
            'resource' => $resource
        ])->firstOrFail();
    }

    /**
     * Find a dynamic app resource item
     * 
     * @param mixed $resourcePKValue - The app resource primary key value
     * @return array
     */
    public function findItem($resourcePKValue) {
        $item = new AppResourceGeneric;
        $item->init($this);
        return $item->find($resourcePKValue);
    }

    /**
     * Find all dynamic app resource items
     * 
     * @return AppResourceGeneric[]
     */
    public function findItems() {
        $item = new AppResourceGeneric;
        $item->init($this);
        return $item->get();
    }

    /**
     * Create a dynamic app resource item
     * 
     * @param array $resourceAttributes - The app resource item attributes (array)
     * @return array
     */
    public function createItem($resourceAttributes) {
        $item = new AppResourceGeneric;
        $item->init($this);
        return $item->create($resourceAttributes);
    }

    /**
     * Update a dynamic app resource item
     * 
     * @param array $resourceItem - The app resource item (array)
     * @return array
     */
    public function updateItem($resourceAttributes) {
        $item = new AppResourceGeneric;
        $item->init($this);
        $toUpdate = $item->find($resourceAttributes); // First or fail
        $toUpdate->init($this);
        // If user related -> check user id
        if ( $this->user_related ) {
            if ($toUpdate->user_id !== Auth::user()->id) {
                abort(403);
            }
        }
        return $toUpdate->update($resourceAttributes);
    }

    /**
     * Delete a dynamic app resource item
     * 
     * @param array $resourceItem - The app resource item (array)
     * @return array
     */
    public function deleteItem($resourceAttributes) {
        $item = new AppResourceGeneric;
        $item->init($this);
        $toDelete = $item->find($resourceAttributes); // First or fail
        $toDelete->init($this);
        // If user related -> check user id
        if ( $this->user_related ) {
            if ($toDelete->user_id !== Auth::user()->id) {
                abort(403);
            }
        }
        return $toDelete->delete();
    }

}
