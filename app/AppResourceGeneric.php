<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Http\Traits\ColumnFillable;

class AppResourceGeneric extends Model
{
    // To dynamically make all fields fillable
    use ColumnFillable;

    // Bypass mass fill assignment guard: declare this protected attribute $guarded as empty array
    protected $guarded = [];

    /**
     * Initialize
     * 
     * @param AppResource $appResource - The AppResource object
     * @return void
     */
    public function init(AppResource $appResource)
    {
        // Set table
        $table = $appResource->table_name;
        $this->setTable($table);
        // Set primary key
        $primKey = $appResource->prim_key;
        $this->primaryKey = $primKey;
        // Set key type
        $keyType = $appResource->key_type;
        $this->keyType = $keyType;
        // Set incrementing
        $incrementing = $appResource->incrementing_value;
        $this->incrementing = $incrementing;
        // Set timestamps
        $timestamps = $appResource->timestamps_fields;
        $this->timestamps = $timestamps;
    }

    /**
     * OVERWRITE Eloquent Model find() method
     * Retrieve a resource by primary key
     * 
     * @param mixed $resourcePKValue - Value of resource primary key
     * @return AppResourceGeneric
     */
    public function find($resourcePKValue) {
        // If primary key value is array
        if ( is_array($this->primaryKey) ) {
            $conditions = [];
            foreach ( $this->primaryKey as $index => $PK ) {
                $conditions[$PK] = $resourcePKValue[$index];
            }
            return $this->where($conditions)->firstOrFail();
        }
        else {
            return $this->where([
                $this->primaryKey => $resourcePKValue
            ])->firstOrFail();
        }
    }

    /**
     * OVERWRITE Eloquent Model create() method
     * Create a resource
     * 
     * @param array $attributes - Resource item attributes
     * @return AppResourceGeneric
     */
    public function create($attributes) {
        foreach ( $attributes as $key => $value ) {
            $this->$key = $value;
        }
        $this->save();
        return $this;
    }

    /**
     * OVERWRITE Eloquent Model update() method
     * Update a resource
     * 
     * @param array $attributes - Resource item attributes
     * @return AppResourceGeneric
     */
    public function update(array $attributes = [], array $options = []) {
        foreach ( $attributes as $key => $value ) {
            $this->$key = $value;
        }
        $this->save();
        return $this;
    }

}
