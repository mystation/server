<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Chumper\Zipper\Zipper;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class App extends Model
{

    // Define the validation rules for the model
    const validationRules = [
        'name' => 'bail|required|unique:apps|max:255',
        'mystation_version' => 'required',
        'version' => 'required',
        'color' => 'required|min:7|max:7'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The primary key column name.
     *
     * @var string
     */
    protected $primaryKey = 'name';

    /**
     * The type of primary key.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The users who have installed the app
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'user_apps', 'app_name', 'user_id')->withPivot(['position', 'is_activated', 'is_initialized']);
    }

    /**
     * The pancakes for the app
     */
    public function pancakes()
    {
        return $this->hasMany('App\AppPancake', 'app_name', 'name');
    }

    /**
     * The resources for the app
     */
    public function resources()
    {
        return $this->hasMany('App\AppResource', 'app_name', 'name');
    }

    /**
     * Get saved resources tables
     */
    public function getTempTables()
    {
        $toReturn = [];
        $tables = DB::select('SHOW TABLES LIKE "temp_'.$this->name.'_%"');
        foreach ($tables as $table) {
            foreach ($table as $key => $value) {
                $matches = [];
                preg_match('/temp_'.$this->name.'_(.*)/', $value, $matches);
                $resourceName = $matches[1];
                $toReturn[$resourceName] = $value;
            }
        }
        return $toReturn;
    }

    /**
     * Get saved resources in temp tables (when updating)
     */
    public function getTempResources()
    {
        $resources = [];
        $tables = $this->getTempTables();
        foreach ($tables as $key => $value) {
            $resources[$key] = DB::select('SELECT * FROM '.$value);
        }
        return $resources;
    }

    /**
     * Drop saved resources tables
     */
    public function dropTempTables()
    {
        $tables = $this->getTempTables();
        foreach ($tables as $resourceName => $tableName) {
            Schema::drop($tableName);
        }
        return true;
    }

    /**
     * Overwrite toArray() method to change name of pivot
     */
    public function toArray()
    {
        $attributes = $this->attributesToArray();
        $attributes = array_merge($attributes, $this->relationsToArray());
        
        // Detect if there is a pivot
        if (isset($attributes['pivot'])) {
            $attributes['app_config'] = $attributes['pivot'];
            unset($attributes['pivot']);
        }

        // Icon encoded data
        $attributes['icon'] = base64_encode($attributes['icon']);
        return $attributes;
    }

    /**
     * Overwrite default 'find' function
     */
    public static function find ($name) {
        return App::where(['name' => $name ])->firstOrFail();
    }

    /**
     * Install all different app files based on the archive
     * @param {string} $name App unique name
     * @param {string} $version App version
     * @param {Stream} $archive App archive (stream)
     * @return {void}
     */
    public static function installFiles ($name, $version, $archive) {        
        $appDir = "/$name";
        $appVersionDir = $appDir . "/" . $version;
        // Remove all existing files
        App::uninstallFiles($name);
        // FIRST : archive
        // Create app folder
        Storage::disk('app_archives')->makeDirectory($appDir);
        // Create version folder
        Storage::disk('app_archives')->makeDirectory($appVersionDir);
        // Create archive file
        $archiveFile = $appVersionDir . "/$name.zip";
        // Put contents
        Storage::disk('app_archives')->put($archiveFile, $archive);
        // THEN : public content
        // Create app folder
        Storage::disk('public')->makeDirectory("/apps" . $appDir);
        // Create version folder
        Storage::disk('public')->makeDirectory("/apps" . $appVersionDir);
        // Read archive
        $zipArchive = App::readArchive($name, $version);
        // Extract content to app public path
        $publicAppFullPath = Storage::disk('public')->getAdapter()->getPathPrefix() . "/apps" . $appVersionDir;
        $zipArchive->extractTo($publicAppFullPath);
    }

    /**
     * Uninstall all different app files
     * @param {string} $name App unique name
     * @return {void}
     */
    public static function uninstallFiles ($name) {
        $appArchivesDiskPath = Storage::disk('app_archives')->getAdapter()->getPathPrefix();
        $publicAppsDiskPath = Storage::disk('public')->getAdapter()->getPathPrefix();
        $appDir = "/$name";
        // FIRST : archive
        // Check if appDir exists in app_archives and remove it
        if (file_exists($appArchivesDiskPath . $appDir)) {
            Storage::disk('app_archives')->deleteDirectory($appDir);
        }
        // THEN : public content
        // Check if appDir exists in public/apps and remove it
        if (file_exists($publicAppsDiskPath . "/apps" . $appDir)) {
            Storage::disk('public')->deleteDirectory("/apps" . $appDir);
        }
    }

    /**
     * Read app archive
     * @param {string} $name App unique name
     * @param {string} $version App version
     * @return {Zipper} Return an instance of Zipper
     */
    public static function readArchive ($name, $version) {
        $zipper = new Zipper();
        $archiveFullPath = Storage::disk('app_archives')->getAdapter()->getPathPrefix() . "/$name/$version/$name.zip";
        return $zipper->zip($archiveFullPath);
    }

    /**
     * Get app.config.json content (JSON format) for a specific app
     * @param {string} $name App unique name
     * @param {string} $version App version
     * @return {array} Return a JSON object (array)
     */
    public static function getJSONConfig ($name, $version) {
        // Store the zipper
        $zipArchive = App::readArchive($name, $version);
        // Get app configuration
        $appConfig = $zipArchive->getFileContent('app.config.json');
        // Decode JSON
        $appConfig = json_decode($appConfig, true);
        return $appConfig;
    }

}
