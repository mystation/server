<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use App\User;
use App\App;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return all users
     * -> Stored in database
     *
     * @return \App\User
     */
    public function all()
    {
        // Return "full" scoped users
        return User::full()->get();
    }

    /**
     * Return authenticated user
     *
     * @return \App\Models\User
     */
    public function me()
    {
        // Return "full" scoped authenticated user
        return User::full()->find(Auth::user()->id);
    }

    /**
     * Logout authenticated user
     */
    public function logout(Request $request)
    {
        Auth::user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Return the found user by unique username
     *
     * @return \App\Models\User
     */
    public function find(Request $request, $username)
    {
        // Return found "full" scoped user
        return User::full()->where('username', $username)->firstOrFail();
    }

    /**
     * Create new user
     */
    public function create(Request $request) {
        // Validate the request
        $this->validate($request, [
            'username' => 'required|unique:users',
            'password' => 'required|string',
            'firstname' => 'required|string',
            'lastname' => 'required|string'
            // 'role' => 'required|in:admin,user'
        ]);
        $newUser = new User();
        // Username: [A-z0-9_] only
        if (preg_match("/^[A-Za-z0-9_]+$/", $request->username)) {
            $newUser->username = $request->username;
        } else {
            return response('Username does not match policy. Try again.', 500);
        }
        $newUser->password = $this->validatePassword($request->password);
        $newUser->firstname = $request->firstname;
        $newUser->lastname = $request->lastname;
        // Default standard... To do in a next version
        // $newUser->role = $request->role;
        $newUser->role = 'user';
        // Save
        $newUser->save();
        // Attach apps
        foreach ( App::all() as $app ) {
            $newUser->apps()->save($app, [
                'is_activated' => true,
                'position' => count($newUser->apps) + 1,
                'is_initialized' => false
            ]);
            // Attach pancake to users
            foreach ( $app->pancakes as $pancake ) {
                $newUser->pancakes()->save($pancake, [
                    'is_activated' => true,
                    'position' => count($newUser->pancakes) + 1
                ]);
            }
        }
        return $newUser;
    }

    /**
     * Delete a user
     */
    public function delete(Request $request, $userId) {
        // Find user
        $user = User::findOrFail($userId);
        // Check if not 'admin' user: in that case return error
        // admin user cannot be deleted
        if ($user->username === 'admin') {
            return response('admin user cannot be deleted', 401);
        }

        // Check if authenticated user is an administrator or match requested user
        if (Auth::user()->isAdmin() || Auth::user()->id === $user->id) {
            $user->delete();
            return response('Success', 200);
        } else {
            return response('Unauthorized', 401);
        }
    }

    /**
     * Validate password
     */
    public function validatePassword($password) {
        // Minimum: 6 characters
        // At least 1 upper case
        // At least 1 lower case
        // At least 1 number
        if (preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,}$/", $password)) {
            $cryptedPassword = Hash::make($password);
            return $cryptedPassword;
        } else {
            return response('Password does not match security policy. Try again.', 500);
        }
    }

    /**
     * Change other user password
     */
    public function resetUserPassword(Request $request, $userId)
    {
        // Validate the request
        $this->validate($request, [
            'newPassword' => 'required|string'
        ]);

        // Check if user exists
        $user = User::findOrFail($userId);

        // Check if not 'admin' user: in that case return error
        // 'admin' account password must be changed via changeAdminPassword method
        if ($user->username === 'admin') {
            return response('Unauthorized', 401);
        }

        // Check if authenticated user is an administrator or match requested user
        if (Auth::user()->isAdmin() || Auth::user()->id === $user->id) {
            $user->password = $this->validatePassword($request->newPassword);
            $user->save();
        } else {
            return response('Unauthorized', 401);
        }

    }

    /**
     * Change admin password
     */
    public function changeAdminPassword(Request $request)
    {
        // Validate the request
        $this->validate($request, [
            'currentPassword' => 'required|string',
            'newPassword' => 'required|string'
        ]);

        // Validate password
        $cryptedPassword = $this->validatePassword($request->newPassword);

        // Check current password and if current password !== new password
        $adminUser = User::where('username', 'admin')->firstOrFail();
        if ( Hash::check($request->currentPassword, $adminUser->password) 
            && !Hash::check($request->newPassword, $adminUser->password)
           ) {

            // Ok, change password
            $adminUser->password = $cryptedPassword;
            $adminUser->save();
            // Execute command on system
            if (env('APP_ENV') === 'production') {
                $process = new Process(["node", "/mystation/bin/cli.js", "admin", "--change-password", str_replace("'", "'\"'\"'", $request->newPassword)]);
                $process->run();
                // executes after the command finishes
                if (!$process->isSuccessful()) {
                    throw new ProcessFailedException($process);
                }
            }
            // If everything ok, update system_data table
            DB::table('system_data')
                ->where('system_key', 'ADMIN_PSSWD_CHANGED')
                ->update(['value' => date("Y-m-d H:i:s")]);

        } else {
            return response('Password is not correct. Try again.', 500);
        }

    }

}
