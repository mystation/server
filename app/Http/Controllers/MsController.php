<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

use App\MyStation;

class MsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return MyStation version
     *
     * @return {array}
     */
    public function version()
    {
        return [
            'system' => MyStation::version(),
            'server' => file_get_contents(base_path('VERSION'))
        ];
    }

    /**
     * Return MyStation network infos
     *
     * @return {array}
     */
    public function network(Request $request)
    {
        return [
            'clientIpAddr' => $request->ip(),
            'sango' => [
                'ipAddr' => $request->server('SERVER_NAME'),
                'port' => $request->server('SERVER_PORT')
            ]
        ];
    }

    /**
     * Return MyStation database informations
     *
     * @return {array}
     */
    public function database()
    {
        $dbName = env('DB_DATABASE');
        $dbSize = DB::select("SELECT SUM(data_length + index_length) / 1024 / 1024 AS 'size' 
            FROM information_schema.TABLES 
            WHERE table_schema = '$dbName' GROUP BY table_schema"
        )[0]->size;// Get only first item
        // Call procedure
        DB::connection()->getPdo()->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
        $results = DB::select("CALL `getDatabaseInfos`();");
        DB::connection()->getPdo()->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        $appsInfos = [];
        // Format returned data
        foreach ($results as $result) {
            if (!isset($appsInfos[$result->app_name])) {
                $appsInfos[$result->app_name] = [];
            }
            $appsInfos[$result->app_name][] = $result;
        }
        return [
            'size' => $dbSize,
            'apps' => $appsInfos
        ];
    }

    /**
     * Return a value stored in system_data
     * @param {string} $key - The data key
     */
    public function getSystemData ($key) {
        return MyStation::getSystemData();
    }

    /**
     * Update system
     */
    public function updateSystem(Request $request)
    {
        // Validate the request
        $this->validate($request, [
            'force' => 'boolean'
        ]);

        $forceMode = $request->force ? $request->force : false;

        // Execute command on system
        if (env('APP_ENV') === 'production') {
            $process = new Process(["sudo", "node", "/mystation/bin/cli.js", "update", $forceMode ? "--force" : ""]);
            $process->run();
            // executes after the command finishes
            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }
        }

        return response('Success', 200);

    }

}
