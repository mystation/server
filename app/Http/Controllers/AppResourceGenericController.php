<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\AppResource;
use App\AppResourceGeneric;

class AppResourceGenericController extends Controller
{

    /**
     * DYNAMIC : Retrieve an unique item for the app resource
     * 
     * @param Request $request - The request
     * @param string $appName - The app unique name
     * @param string $resource - The app resource name (unique for the app)
     * @param string $resourcePKValue - The app resource Primary Key value to find
     * 
     * @return mixed
     */
    public function find(Request $request, $appName, $resource, $resourcePKValue) {
        // First, find AppResource
        $appResource = AppResource::find($appName, $resource); // First or fail
        // Based on the app resource, find unique item by PK
        $item = $appResource->findItem($resourcePKValue); // First or fail
        // Return the item
        return $item;
    }

    /**
     * DYNAMIC : Retrieve all items for the app resource
     * 
     * @param Request $request - The request
     * @param string $appName - The app unique name
     * @param string $resource - The app resource name (unique for the app)
     * 
     * @return mixed
     */
    public function all(Request $request, $appName, $resource) {
        // First, find AppResource
        $appResource = AppResource::find($appName, $resource); // First or fail
        // Based on the app resource, find items
        $items = $appResource->findItems();
        // Return the item
        return $items;
    }

    /**
     * DYNAMIC : Create an item for the app resource
     * 
     * @param Request $request - The request
     * @param string $appName - The app unique name
     * @param string $resource - The app resource name (unique for the app)
     * 
     * @return mixed
     */
    public function create(Request $request, $appName, $resource) {
        // First, find AppResource
        $appResource = AppResource::find($appName, $resource); // First or fail
        // Get parameters
        $item = $request->toArray();
        // If user related -> add user_id field with value: authenticated user
        if ( $appResource->user_related ) {
            $item['user_id'] = Auth::user()->id;
        }
        $item = $appResource->createItem($item); // Create or fail
        // Return the item
        return $item;
    }

    /**
     * DYNAMIC : Update an item for the app resource
     * 
     * @param Request $request - The request
     * @param string $appName - The app unique name
     * @param string $resource - The app resource name (unique for the app)
     * 
     * @return mixed
     */
    public function update(Request $request, $appName, $resource) {
        // First, find AppResource
        $appResource = AppResource::find($appName, $resource); // First or fail
        // Get parameters
        $item = $request->toArray();
        $item = $appResource->updateItem($item); // Update or fail
        // Return the item
        return $item;
    }

    /**
     * DYNAMIC : Delete an item for the app resource
     * 
     * @param Request $request - The request
     * @param string $appName - The app unique name
     * @param string $resource - The app resource name (unique for the app)
     * 
     * @return mixed
     */
    public function delete(Request $request, $appName, $resource) {
        // First, find AppResource
        $appResource = AppResource::find($appName, $resource); // First or fail
        // Get parameters
        $item = $request->toArray();
        $item = $appResource->deleteItem($item); // Delete or fail
    }
}
