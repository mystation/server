<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use \Image;
use App\App as MsApp;
use App\AppResource;
use App\User;
use App\AppPancake;
use App\MyStation;

class AppController extends Controller
{

    /**
     * Return all apps
     * -> Stored in database
     *
     * @return MsApp[]
     */
    public function index()
    {
        // Return all apps
        return MsApp::with(['pancakes', 'resources'])->get();
    }

    /**
     * Init the app for the user
     *
     * @param Request $request - The request
     * @param string $name - The app name
     * 
     * @return Boolean
     */
    public function init(Request $request, $name)
    {
        // Found app
        $app = MsApp::findOrFail($name);
        // Modify pivot property: 'is_initialized'
        Auth::user()->apps()
                    ->wherePivot('app_name', $name)
                    ->wherePivot('user_id', Auth::user()->id)
                    ->update([
                        'is_initialized' => true
                    ]);
    }

    /**
     * Return the found app by unique name
     *
     * @param Request $request - The request
     * @param string $name - The app name
     * 
     * @return MsApp
     */
    public function find(Request $request, $name)
    {
        // Return the found MsApp
        return MsApp::findOrFail($name);
    }

    /**
     * Store an App in database
     *
     * @param Request $request - The request
     * @param bool [$update = false] - If app update
     * @param string [$fromVersion = null] - The old version for app update
     * @param bool [$development = false] - Defines if app development mode enabled
     * @param string [$devAppIconURL = null] - In case of app development mode enabled, contains the URL for app.icon.png
     * 
     * @return mixed
     */
    public function store(Request $request, bool $update = false, string $fromVersion = null, bool $development = false, string $devAppIconURL = null)
    {
        // Validate the App configuration 
        $this->validate($request, MsApp::validationRules);
        // Create a new App
        $app = new MsApp;
        // Assign validated properties
        $app_name = $request->name;
        // App name
        $app->name = $app_name;
        // App title
        $app->title = $request->title;
        // App version
        $app->version = $request->version;
        // MyStation version
        $app->mystation_version = $request->mystation_version;
        // App icon
        if ($development) {
            // URL for icon
            $iconPath = $devAppIconURL;
            // Http client
            $client = new Client();
            // Get app.icon.png from dev URL
            $res = $client->request('GET', $iconPath);
            $blob = $res->getBody();
        } else {
            // Icon path in the archive
            $iconPath = 'app.icon.png';
            // Get icon file content
            $blob = MsApp::readArchive($app->name, $app->version)->getFileContent($iconPath);
        }
        // Create an Image with icon data
        $tmp_img = Image::make($blob);
        // Check if image is .png format
        if ( $tmp_img->mime() === 'image/png' ) {
            // Resize image -> App icon must be squared format 
            $tmp_img->resize(200, 200); // Resize to 200x200 px default size
            // Assign to app
            $app->icon = $tmp_img->stream('png', 100);
        }
        else {
            abort(500, 'App icon must be in PNG format');
        }
        // App color
        $app->color = $request->color;
        // If resources are declared
        $resources = $request['resources'] ? $request['resources'] : [];
        $appResourceCollection = [];
        // For each resources
        foreach ( $resources as $index => $resource ) {
            // Create new AppResource
            $appResource = new AppResource;
            $appResource->app_name = $app_name;
            $appResource->resource = $resource['table'];
            $appResource->table_name = $app_name . '_' . $resource['table'];
            $appResource->prim_key = $resource['primaryKey'];
            if (!is_array($appResource->prim_key)) {
                $appResource->key_type = $resource['keyType'];
            }
            $appResource->incrementing_value = isset($resource['incrementing']) ? $resource['incrementing'] : true;
            $appResource->timestamps_fields = isset($resource['timestamps']) ? $resource['timestamps'] : true;
            $appResource->user_related = isset($resource['userRelated']) ? $resource['userRelated'] : true;
            // Foreigns
            $foreigns = isset($resource['foreigns']) ? $resource['foreigns'] : [];
            // Create table
            $appResource->createTable($resource['columns'], $foreigns);
            // Add resources to collection
            $appResourceCollection[] = $appResource;
        }
        // Persist app in database
        $app->save();
        // Retrieve saved App because save() method return boolean
        $app = MsApp::find($app_name);
        // Pancakes
        if ($request->pancakes) {
            if (is_array($request->pancakes)) {
                // For each pancake
                foreach ( $request->pancakes as $index => $pancakeName ) {
                    $safePancakeName = $app->name . '-' . $pancakeName;
                    $pancake = new AppPancake([
                        'name' => $safePancakeName,
                        'app_name' => $app->name
                    ]);
                    $pancake->save();
                    // Retrieve saved Pancake because save() method return boolean
                    $pancake = AppPancake::find($safePancakeName);
                    // Attach pancake to users
                    foreach ( User::all() as $user ) {
                        $user->pancakes()->save($pancake, [
                            'is_activated' => true,
                            'position' => count($user->pancakes) + 1
                        ]);
                    }
                }
            } else {
                abort(500, 'App pancakes must be an array of string');
            }
        }
        // Attach app to users
        foreach ( User::all() as $user ) {
            $app->users()->attach($user, [
                'is_activated' => true,
                'position' => count($user->apps) + 1,
                'is_initialized' => false
            ]);
        }
        // Attach each resource to new app
        foreach ( $appResourceCollection as $AR ) {
            $app->resources()->create($AR->toArray());
        }
        // Return the created app
        if ($update) {
            return [
                'app' => $app,
                'updatedFrom' => $fromVersion,
                'updatedTo' => $app->version,
                'tempResources' => $app->getTempResources()
            ];
        } else {
            return $app;
        }
    }

    /**
     * Install the MsApp
     *
     * @param Request $request - The request
     * 
     * @return MsApp
     */
    public function install(Request $request)
    {   
        // Validate the needed data
        $this->validate($request, [
            'name' => 'required|string',
            'version' => 'required|string',
            'update' => 'boolean',
            'development' => 'boolean',
            'dev_app_config_url' => 'string|nullable',
            'dev_app_icon_url' => 'string|nullable'
        ]);
        // App name
        $appName = $request->name;
        // If app development mode
        if ($request->development) {
            // Http client
            $client = new Client();
            // Get app.config.json from dev URL
            $res = $client->request('GET', $request->dev_app_config_url);
            // If success
            if ($res->getStatusCode() === 200) {
                // Create new request with extracted configuration
                $requestToRedirect = new Request(json_decode($res->getBody(), true));
                // Set necessary headers to forward request to 'store' method
                // -> fix the 302 redirection issue in 'store' method caused by headers missing
                $requestToRedirect->headers->set('Content-Type', 'application/json');
                $requestToRedirect->headers->set('Accept', 'application/json');
                // Store App
                return $this->store($requestToRedirect, false, null, true, $request->dev_app_icon_url);
            } else {
                abort(404);
            }
        }
        // Update ?
        $update = $request->update ? $request->update : false;
        // If update
        if ($update) {
            // old version
            $fromVersion = MsApp::findOrFail($appName)->version;
        } else {
            $fromVersion = null;
        }
        // new version
        $toVersion = $request->version;
        // Format for url: replace dots ('.') by underscores ('_')
        $appVersion = str_replace(".", "_", $request->version);
        // Http client
        $client = new Client();
        // Request MyStation Web API
        $res = $client->request('GET', env('MYSTATION_WEB_API_URL') . "/apps/$appName/v/$appVersion/archive");
        // If success
        if ($res->getStatusCode() === 200) {
            // If update, duplicate app resources tables
            if ($update) {
                $this->duplicateResourcesTables($request);
                $this->uninstall($request);
            }
            // Call installFiles static method to install files in their respective folders:
            // - archive file
            // - 'dist' directory content
            MsApp::installFiles($appName, $toVersion, $res->getBody());
            // Get JSON app config
            $appConfig = MsApp::getJSONConfig($appName, $toVersion);
            // Control MyStation version
            if (strcmp(MyStation::version(), $appConfig['mystation_version']) === -1) {
                abort(500, 'MyStation version : '.MyStation::version().', required : '.$appConfig['mystation_version']);
            }
            // Create new request with extracted configuration
            $requestToRedirect = new Request($appConfig);
            // Set necessary headers to forward request to 'store' method
            // -> fix the 302 redirection issue in 'store' method caused by headers missing
            $requestToRedirect->headers->set('Content-Type', 'application/json');
            $requestToRedirect->headers->set('Accept', 'application/json');
            // Store App
            return $this->store($requestToRedirect, $update, $fromVersion);
        } else {
            abort(404);
        }
    }

    /**
     * Uninstall the MsApp
     *
     * @param Request $request - The request
     * 
     * @return void
     */
    public function uninstall(Request $request)
    {   
        // Validate the app name
        $this->validate($request, [
            'name' => 'bail|required|string'
        ]);
        // Find app
        $app = MsApp::with('resources')->find($request->name);
        // Drop resources tables
        foreach ( $app->resources as $resource ) {
            $resource->deleteTable();
        }
        // Delete AppResources
        AppResource::where([
            'app_name' => $app->name
        ])->delete();
        // Uninstall app files
        MsApp::uninstallFiles($request->name);
        // Delete App
        return MsApp::destroy($app->name);
    }

    /**
     * Dulicate app resources tables for update
     *
     * @param Request $request - The request
     * 
     * @return bool
     */
    public function duplicateResourcesTables(Request $request)
    {   
        // Validate the app name
        $this->validate($request, [
            'name' => 'bail|required|string'
        ]);
        // Find app
        $app = MsApp::with('resources')->findOrFail($request->name);
        // Drop resources tables
        foreach ( $app->resources as $resource ) {
            $resource->tempTable(); // Create temp table for update
        }
        // Delete App
        return true;
    }

    /**
     * Reload database table after update
     *
     * @param Request $request - The request
     * 
     * @return void
     */
    public function updateResources(Request $request)
    {
        // Validate the app name
        $this->validate($request, [
            'name' => 'bail|required|string'
        ]);
        $appName = $request->name;
        // Find app
        $app = MsApp::findOrFail($appName);
        // For each resource
        foreach ($request->resources as $resourceName => $collection) {
            // Check if resource exists
            $appResource = AppResource::find($appName, $resourceName);
            // Insert collection
            DB::table($appResource->table_name)->insert($collection);
        }
        // Delete temp tables
        $app->dropTempTables();
    }

}
