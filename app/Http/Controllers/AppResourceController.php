<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AppResource;

class AppResourceController extends Controller
{
    /**
     * Return all app resources
     *
     * @return AppResource[]
     */
    public function index()
    {
        // Return all app resources
        return AppResource::all();
    }

    /**
     * Return all resources by app
     *
     * @param string $appName - The app unique name
     * @return AppResource[]
     */
    public function allByApp($appName)
    {
        // Return all app resources
        return AppResource::where([
            'app_name' => $appName
        ])->get();
    }

    /**
     * Return an app resource
     *
     * @param Request $request - The request
     * @param string $appName - The app unique name
     * @return AppResource
     */
    public function find(Request $request, $appName)
    {
        // Validate the resource name
        $this->validate($request, [
            'resource' => 'bail|required|string',
        ]);
        // Return all app resources
        return AppResource::where([
            'app_name' => $appName,
            'resource' => $request['resource']
        ])->firstOrFail();
    }

}
