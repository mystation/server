<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\DB;

use Closure;

class IsAdminPsswdChanged
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( DB::table('system_data')->where('system_key', 'ADMIN_PSSWD_CHANGED')->get()[0]->value !== null ){
            return $next($request);
        }
        return response([
            'forbidden' => [
                'ADMIN_PSSWD_CHANGED' => false
            ]
        ], 403);
    }
}
