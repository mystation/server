<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password', 'firstname', 'lastname'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * Scope a query to include full users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFull($query)
    {
        return $query->with(['apps', 'pancakes']);
    }

    /**
     * Return true if the user is admin
     * 
     */
    public function isAdmin(){
        if($this->role == "admin"){
            return true;
        }
        return false;
    }

    /** 
     * To connect oAuth API with username instead of email
     * 
     * @param ??? $identifier
     * @return App\User
     */
    public function findForPassport($identifier) {
        return $this->where('username', $identifier)->first();
    }

    /**
     * Return last user login date (based on last token)
     */
    public function lastLogin() {
        // test comment
        $lastToken = DB::table('oauth_access_tokens')->where('user_id', $this->id)->latest()->first();
        return $lastToken ? $lastToken->created_at : null;
    }

    /**
     * Overwrite toArray() method
     */
    public function toArray()
    {
        $attributes = $this->attributesToArray();
        $attributes = array_merge($attributes, $this->relationsToArray());
        // Last login
        $attributes['last_login'] = $this->lastLogin();
        return $attributes;
    }

    /**
     * The apps installed by the user
     * 
     * @return array of App\App
     */
    public function apps()
    {
        return $this->belongsToMany('App\App', 'user_apps', 'user_id', 'app_name')->withPivot(['position', 'is_activated', 'is_initialized']);
    }

    /**
     * The pancakes activated by the user
     * 
     * @return array of App\AppPancake
     */
    public function pancakes()
    {
        return $this->belongsToMany('App\AppPancake', 'user_pancakes', 'user_id', 'pancake_name')->withPivot(['position', 'is_activated']);
    }


}
