<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class MyStation
{

    /**
     * Returns system data
     */
    public static function getSystemData () {
        $toReturn = [];
        $data = DB::select("SELECT * FROM system_data");
        foreach ($data as $row) {
            $toReturn[$row->system_key] = $row->value;
        }
        return $toReturn;
    }

    /**
     * Returns MyStation version
     */
    public static function version () {
        return MyStation::getSystemData()['VERSION'];
    }

}