<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use \Chumper\Zipper\Zipper;

class UpdateUI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ui:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update UI compiled sources from gitlab artifact';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Cleaning 'public' folder...\n";
        // First, remove all files except $exclude files
        $exclude = [
            'index.php',
            '.htaccess',
            'storage'
        ];
        $exclude = join(',', $exclude);
        $exclude = glob(public_path() . "/{".$exclude."}", GLOB_BRACE);
        $allFiles = glob(public_path() . "/{*,.[!.]*,..?*}", GLOB_BRACE);
        $toDelete = array_diff($allFiles, $exclude);
        foreach ($toDelete as $fileOrDir) {
            if (is_dir($fileOrDir)) {
                \File::deleteDirectory($fileOrDir);
            } else {
                \File::delete($fileOrDir);
            }
        }
        echo "'public' folder reinitialized\n";
        // Http client
        $client = new Client();
        $uiArtifactURL = "https://gitlab.com/mystation/ui/-/jobs/artifacts/master/download?job=build";
        echo "Request $uiArtifactURL\n";
        $res = $client->request('GET', $uiArtifactURL, [
            'verify' => false
        ]);
        // If success
        if ($res->getStatusCode() === 200) {
            // Archive temp path
            $archivePath = public_path() . '/ui_temp.zip';
            // Write archive
            \File::put($archivePath, $res->getBody());
            // New Zipper
            $zipper = new Zipper();
            // Unzip 'dist' folder into public
            $zipper->zip($archivePath)->folder('dist')->extractTo(public_path());
            // Remove temp zip file
            \File::delete($archivePath);
        } else {
            echo "An error has ocurred...";
        }
    }
}
