# MyStation - Back server (Web API)

MyStation v1.0.0
----------------
- PHP >= v7.1.3
- mysql v10.1.38-MariaDB (Raspbian)
- Laravel Framework v5.4.36

## Get started

> Please check the 'root' repository before clone this one
> [https://gitlab.com/mystation/mystation](https://gitlab.com/mystation/mystation)

```bash
# Install vendor
composer initialize
# Take a coffee ;)
```

### Environment

Copy the ".env.example" file and rename it to ".env".

Configure it:

```env
...
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
...
```

See Laravel documentation for more information: [https://laravel.com/docs/5.4/configuration](https://laravel.com/docs/5.4/configuration)

### Get UI sources (last Gitlab artifact)

```bash
php artisan ui:update
```

### Symbolic link for apps public path

```bash
php artisan storage:link
```

### Start server

```bash
php artisan serve
# or
php -S localhost:<port> -t public
```

### Output routes in Markdown table format

```bash
# Takes the same options as 'php artisan route:list'
php artisan route:mdtable --sort=uri
```
