<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_resources', function (Blueprint $table) {
            $table->string('app_name')->comment('App name');
            $table->foreign('app_name')
                  ->references('name')
                  ->on('apps')
                  ->onDelete('cascade');
            $table->string('resource')->comment("The app resource name");
            $table->string('table_name')->comment('The app resource table name');
            $table->string('prim_key')->comment('The app resource table primary key name');
            $table->enum('key_type', [
                'int', 'string'
            ])->comment('The app resource table primary key type');
            $table->boolean('incrementing_value')->default(true)->comment('Determine if the primary key is an integer to auto-increment');
            $table->boolean('timestamps_fields')->default(true)->comment('Determine if table has timestamps fields');
            $table->boolean('user_related')->default(true)->comment('Determine if table has user_id related field');
            $table->primary(['app_name', 'resource']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('app_resources');
        Schema::enableForeignKeyConstraints();
    }
}
