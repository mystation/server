<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_apps', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->comment('User ID');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->string('app_name')->comment('App name');
            $table->foreign('app_name')
                ->references('name')
                ->on('apps')
                ->onDelete('cascade');
            $table->unsignedInteger('position')->comment('App position');
            $table->boolean('is_activated')->comment('If app is activated for the user');
            $table->boolean('is_initialized')->comment('If app data is initialized, or for updates...');
            $table->primary(array('user_id', 'app_name'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('user_apps');
        Schema::enableForeignKeyConstraints();
    }
}
