<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPancakesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_pancakes', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->comment('User ID');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->string('pancake_name')->comment('App pancake name');
            $table->foreign('pancake_name')
                ->references('name')
                ->on('app_pancakes')
                ->onDelete('cascade');
            $table->unsignedInteger('position')->comment('App pancake position on the dashboard');
            $table->boolean('is_activated');
            $table->primary(array('user_id', 'pancake_name', 'position'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('user_pancakes');
        Schema::enableForeignKeyConstraints();
    }
}
