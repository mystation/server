<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->unique()->comment("Username");
            $table->string('password')->comment("Password encrypted with bcrypt");
            $table->string('firstname')->comment("Firstname");
            $table->string('lastname')->comment("Lastname");
            $table->date('birth_date')->comment("Birth date");
            $table->enum('role', ['admin', 'user'])->default('user')->comment("User role ('admin' or 'user')");
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('users');
        Schema::enableForeignKeyConstraints();
    }
}
