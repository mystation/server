<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppPancakesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_pancakes', function (Blueprint $table) {
            $table->string('name')->comment("The app pancake name");
            $table->string('app_name')->comment('App name');
            $table->foreign('app_name')
                  ->references('name')
                  ->on('apps')
                  ->onDelete('cascade');
            $table->primary('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('app_pancakes');
        Schema::enableForeignKeyConstraints();
    }
}
