<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMytasksTodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('mytasks_todos', function (Blueprint $table) {
        //     $table->increments('id')->unique()->comment("ToDo ID");
        //     $table->string('label')->comment("ToDo label");
        //     $table->enum('type', ['standard', 'recurring', 'onetime'])->default('standard')->comment("ToDo type ('standard', 'recurring' or 'onetime')");
        //     $table->enum('status', ['todo', 'done', 'pending'])->default('todo')->comment("ToDo status ('todo', 'done' or 'pending')");
        //     $table->string('recurrence')->nullable()->comment("ToDo recurrence");
        //     $table->unsignedInteger('user_id')
        //         ->foreign('user_id')
        //         ->references('id')
        //         ->on('users');
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('mytasks_todos');
    }
}
