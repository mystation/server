<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apps', function (Blueprint $table) {
            $table->string('name')->unique()->comment("App name");
            $table->string('title')->comment("App displayed name");
            $table->string('version')->comment("App version in format X.X.X");
            $table->string('mystation_version')->comment("MyStation min version in format X.X.X");
            $table->binary('icon')->nullable()->comment("App icon");
            $table->string('color', 7)->comment("App color value in hexadecimal format with the '#' (length: 7)");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apps');
    }
}
