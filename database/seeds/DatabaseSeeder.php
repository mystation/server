<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Users
        $this->call(UsersTableSeeder::class);
        // Apps
        $this->call(AppsTableSeeder::class);
        // TEST FOR DYNAMIC APP RESOURCE TABLE
        $this->call(MytasksTodosTableSeeder::class);
        $this->call(AppResourcesTableSeeder::class);
        // User apps
        $this->call(UserAppsTableSeeder::class);
        // OAuth
        $this->call(OAuthClientsTableSeeder::class);
        $this->call(OAuthPersonalAccessClientsTableSeeder::class);
    }
}
