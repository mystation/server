<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin
        User::create([
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'firstname' => '',
            'lastname' => '',
            'birth_date' => '1993-11-12',
            'role' => 'admin'
        ]);

    }
}