<?php

use Illuminate\Database\Seeder;

class OAuthClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->insert([
            [
                'id' => 1,
                'name' => "MyStation API Password Grant Client",
                // 'secret' => "ZZPyaEhcPXQItuLKpQYkjyKu8sow4zSBapzJdYoT",
                'secret' => "My5t4ti0n",
                'redirect' => "http://localhost",
                'personal_access_client' => "0",
                'password_client' => "1",
                'revoked' => "0",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'id' => 2,
                'name' => "MyStation API Personal Access Client",
                'secret' => "SC0ltpGJTWAxTXVlMBDgdFJgH0L0rgOJyFn6M7EL",
                'redirect' => "http://localhost",
                'personal_access_client' => "1",
                'password_client' => "0",
                'revoked' => "0",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        ]);
    }
}