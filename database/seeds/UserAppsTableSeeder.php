<?php

use Illuminate\Database\Seeder;

class UserAppsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_apps')->insert([
            // Activate all apps for admin
            // ['app_name' => 'myfeeds', 'user_id' => 1, 'position' => 1, 'is_activated' => true]
            // ['app_name' => 'mylinks', 'user_id' => 1, 'position' => 2, 'is_activated' => false]
            // ['app_name' => 'mytasks', 'user_id' => 1, 'position' => 3, 'is_activated' => true]
        ]);

    }
}