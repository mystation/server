<?php

use Illuminate\Database\Seeder;
use App\AppResource;

class AppResourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // // MyTasks todos
        // AppResource::create([
        //     'app_name' => 'mytasks',
        //     'resource' => 'todos',
        //     'table_name' => 'mytasks_todos',
        //     'prim_key' => 'id',
        //     'timestamps_fields' => true,
        //     'user_related' => true
        // ]);
        // // Mynotes notes
        // AppResource::create([
        //     'app_name' => 'mynotes',
        //     'resource' => 'notes',
        //     'table_name' => 'mynotes_notes',
        //     'prim_key' => 'id',
        //     'timestamps_fields' => true,
        //     'user_related' => true
        // ]);
        // // MyFeeds feeds
        // AppResource::create([
        //     'app_name' => 'myfeeds',
        //     'resource' => 'feeds',
        //     'table_name' => 'myfeeds_feeds',
        //     'prim_key' => 'id',
        //     'user_related' => true
        // ]);
    }
}