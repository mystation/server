<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Change admin password
Route::post('/changeAdminPassword', 'UserController@changeAdminPassword');

// Users
Route::group( [ 'middleware' => [ 'auth:api', 'admin_psswd_changed' ] ] , function () use ($router) {
    $router->get('/me', 'UserController@me');
    $router->post('/me/logout', 'UserController@logout')->name('logout');
    // MyStation version
    Route::get('/system/version', 'MsController@version')->name('version');
    // Network infos
    Route::get('/system/network', 'MsController@network')->name('network');
    // Database infos
    Route::get('/system/database', 'MsController@database')->name('database');
    // Users
    Route::get('/users', 'UserController@all')->name('users');
    Route::post('/users/{userId}/delete', 'UserController@delete');
    Route::post('/users/{userId}/resetPassword', 'UserController@resetUserPassword');
    // Apps
    Route::get('/apps', 'AppController@index')->name('apps');
    Route::get('/apps/{name}', 'AppController@find')->name('app');
    // Init app
    Route::post('/apps/{name}/init', 'AppController@init')->name('app.init');
    // Apps resources
    // Route::get('/apps/resources', 'AppResourceController@index')->name('app.resources.all');
    Route::get('/apps/{name}/resources', 'AppResourceController@allByApp')->name('app.resources.byApp');
    Route::post('/apps/{name}/resources', 'AppResourceController@find')->name('app.resources.find');
    // Apps resources items
    Route::get('/apps/{name}/resources/{resource}', 'AppResourceGenericController@all')->name('app.resource.all');
    Route::post('/apps/{name}/resources/{resource}', 'AppResourceGenericController@create')->name('app.resource.create');
    Route::get('/apps/{name}/resources/{resource}/{pkval}', 'AppResourceGenericController@find')->name('app.resource.find');
    Route::patch('/apps/{name}/resources/{resource}/', 'AppResourceGenericController@update')->name('app.resource.update');
    Route::delete('/apps/{name}/resources/{resource}/', 'AppResourceGenericController@delete')->name('app.resource.delete');
});

//// ADMIN

Route::group( [ 'middleware' => ['auth:api', 'admin', 'admin_psswd_changed' ] ] , function () use ($router) {
    // Update system
    Route::post('/system/update', 'MsController@updateSystem')->name('update');
    // Users
    Route::post('/users/create', 'UserController@create');
    // App installation
    $router->post('/apps/install', 'AppController@install');
    // App uninstallation
    $router->post('/apps/uninstall', 'AppController@uninstall');
    // App activation
    $router->post('/apps/activate', 'AppController@activate');
    // App deactivation
    $router->post('/apps/deactivate', 'AppController@deactivate');
    // App update
    $router->post('/apps/updateResources', 'AppController@updateResources'); // POST: receive json resources for 'to' version
});
